<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\CategoryCreated;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;
use Symfony\Component\Mime\Email;


class CategoryEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailerInterface $mailer,
        private Environment $engine
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CategoryCreated::class => 'onCategoryCreated',
        ];
    }

    public function onCategoryCreated(CategoryCreated $event): void
    {
        $email = (new Email())
            ->from('admin@api.com')
            ->to('laurenssanchez24@gmail.com_create_guid')
            ->subject('Nueva categoria creada!')
            ->text('Nueva categoria creada!')
            ->html(
                $this->engine->render('emails/new-category.html.twig', [
                    'id' => $event->id,
                    'name' => $event->name,
                    'created_on' => $event->createdAt,
                ])
            );

        $this->mailer->send($email);
    }
}