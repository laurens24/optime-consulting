<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Category;


#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

   #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[Assert\Length(
        min: 4,
        max: 10,
        minMessage: 'El codigo del producto debe ser al menos {{ limit }} caracteres',
        maxMessage: 'El codigo del producto debe ser de máximo {{ limit }} caracteres'
    )]
    #[ORM\Column(type: 'string', length: 20)]
    private ?string $code;

    
   #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[Assert\Length(
        min: 4,
        max: 100,
        minMessage: 'El nombre del producto debe ser al menos {{ limit }} caracteres',
        maxMessage: 'El nombre del producto debe ser de máximo {{ limit }} caracteres'
    )]
    #[ORM\Column(type: 'string', length: 100)]
    private ?string $name;



    #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[ORM\Column(type: 'text')]
    private ?string $description;


    #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[ORM\Column(type: 'string')]
    private ?string $brand;


   #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private Category $category;

    
   #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'integer')]
    private ?int $price;


    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    // #[ORM\Column(type: 'datetime')]
    // private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = strtoupper($name);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = strtoupper($brand);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    // public function getUpdatedAt(): ?\DateTimeInterface
    // {
    //     return $this->updatedAt;
    // }

    // public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    // {
    //     $this->updatedAt = $updatedAt;

    //     return $this;
    // }
}
