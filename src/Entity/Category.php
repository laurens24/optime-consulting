<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Assert\NotBlank(message:'el campo no puede estar vacío')]
    #[Assert\Length(
        min: 4,
        max: 100,
        minMessage: 'El nombre de la categoria debe ser al menos {{ limit }} caracteres',
        maxMessage: 'El nombre de la categoria debe ser máximo {{ limit }} caracteres'
    )]
    
    #[ORM\Column(type: 'string', length: 100)]
    private ?string $name;


    #[ORM\Column(type: 'boolean')]
    private $active;


    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createdAt;


    // #[ORM\Column(type: 'datetime')]
    // private ?\DateTimeInterface $updatedAt;


    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Product::class)]
    private Collection  $products;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = strtoupper($name);

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    // public function getUpdatedAt(): ?\DateTimeInterface
    // {
    //     return $this->updatedAt;
    // }

    // public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    // {
    //     $this->createdAt = $updatedAt;
    //     return $this;
    // }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCategory() === $this) {
                $product->setCategory(null);
            }
        }

        return $this;
    }

}
